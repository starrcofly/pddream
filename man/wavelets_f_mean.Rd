\name{wavelets_f_mean}
\alias{wavelets_f_mean}

\title{
    wavelets_f_mean
}

\description{
    wavelets_f_mean
}

\usage{
 wavelets_f_mean(v)
}

\arguments{
   \item{v}{}
}

\author{
 Xinyu Zhang <xinyu.zhang@yale.edu>
 Ying Hu <yhu@mail.nih.gov>
}

\references{
  ##
}

\examples{
  ##
}

