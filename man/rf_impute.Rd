\name{rf_impute}
\alias{rf_impute}

\title{
    rf_impute
}

\description{
    rf_impute
}

\usage{
 rf_impute(dat, response)
}

\arguments{
   \item{dat}{}
   \item{response}{}
}

\author{
 Xinyu Zhang <xinyu.zhang@yale.edu>
 Ying Hu <yhu@mail.nih.goout, l>
}

\references{
  ##
}

\examples{
  ##
}

