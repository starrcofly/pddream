\name{wavelets_f_max}
\alias{wavelets_f_max}

\title{
    wavelets_f_max
}

\description{
    wavelets_f_max
}

\usage{
 wavelets_f_max(v)
}

\arguments{
   \item{v}{}
}

\author{
 Xinyu Zhang <xinyu.zhang@yale.edu>
 Ying Hu <yhu@mail.nih.gov>
}

\references{
  ##
}

\examples{
  ##
}

